-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 12, 2017 at 04:12 PM
-- Server version: 5.6.33
-- PHP Version: 5.6.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `devserve_gomo`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers_de`
--

CREATE TABLE `answers_de` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_text` varchar(255) NOT NULL,
  `solution` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers_de`
--

INSERT INTO `answers_de` (`answer_id`, `question_id`, `answer_text`, `solution`) VALUES
(1, 1, 'Brighton', 1),
(2, 1, 'Sheffield', 0),
(3, 1, 'London', 0),
(4, 2, 'Der afrikanische Elefant', 0),
(5, 2, 'Der blaue Wal', 1),
(6, 2, 'Der Whale Shark', 0),
(7, 2, 'Das Great Barrier Reef', 0),
(8, 2, 'Der Elefantenspitzmaus', 0);

-- --------------------------------------------------------

--
-- Table structure for table `answers_es`
--

CREATE TABLE `answers_es` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_text` varchar(255) NOT NULL,
  `solution` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers_es`
--

INSERT INTO `answers_es` (`answer_id`, `question_id`, `answer_text`, `solution`) VALUES
(1, 1, 'Brighton', 1),
(2, 1, 'Sheffield', 0),
(3, 1, 'Londres', 0),
(4, 2, 'El elefante africano', 0),
(5, 2, 'La ballena azul', 1),
(6, 2, 'El tiburón ballena', 0),
(7, 2, 'La gran Barrera de Coral', 0),
(8, 2, 'La Musaraña de Elefantes', 0);

-- --------------------------------------------------------

--
-- Table structure for table `answers_fr`
--

CREATE TABLE `answers_fr` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_text` varchar(255) NOT NULL,
  `solution` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers_fr`
--

INSERT INTO `answers_fr` (`answer_id`, `question_id`, `answer_text`, `solution`) VALUES
(1, 1, 'Brighton', 1),
(2, 1, 'Sheffield', 0),
(3, 1, 'Londres', 0),
(4, 2, 'L\'éléphant d\'Afrique', 0),
(5, 2, 'La baleine bleue', 1),
(6, 2, 'The Whale Shark', 0),
(7, 2, 'La grande Barrière de corail', 0),
(8, 2, 'The Elephant Shrew', 0);

-- --------------------------------------------------------

--
-- Table structure for table `answers_jp`
--

CREATE TABLE `answers_jp` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_text` varchar(255) NOT NULL,
  `solution` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers_jp`
--

INSERT INTO `answers_jp` (`answer_id`, `question_id`, `answer_text`, `solution`) VALUES
(1, 1, 'ブライトン', 1),
(2, 1, 'シェフィールド', 0),
(3, 1, 'ロンドン', 0),
(4, 2, 'アフリカゾウ', 0),
(5, 2, '青い鯨', 1),
(6, 2, 'ホエールサメ', 0),
(7, 2, 'グレートバリアリーフ', 0),
(8, 2, 'エレファントシュルー', 0);

-- --------------------------------------------------------

--
-- Table structure for table `answers_uk`
--

CREATE TABLE `answers_uk` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_text` varchar(255) NOT NULL,
  `solution` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers_uk`
--

INSERT INTO `answers_uk` (`answer_id`, `question_id`, `answer_text`, `solution`) VALUES
(1, 1, 'Brighton', 1),
(2, 1, 'Sheffield', 0),
(3, 1, 'London', 0),
(4, 2, 'The African Elephant', 0),
(5, 2, 'The Blue Whale', 1),
(6, 2, 'The Whale Shark', 0),
(7, 2, 'The Great Barrier Reef', 0),
(8, 2, 'The Elephant Shrew', 0);

-- --------------------------------------------------------

--
-- Table structure for table `countries_de`
--

CREATE TABLE `countries_de` (
  `id` int(11) NOT NULL,
  `country_code` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries_de`
--

INSERT INTO `countries_de` (`id`, `country_code`, `country_name`) VALUES
(1, 'uk', 'Großbritannien'),
(2, 'de', 'Deutschland'),
(3, 'fr', 'Frankreich'),
(4, 'es', 'Spanien'),
(5, 'jp', 'Japan');

-- --------------------------------------------------------

--
-- Table structure for table `countries_es`
--

CREATE TABLE `countries_es` (
  `id` int(11) NOT NULL,
  `country_code` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries_es`
--

INSERT INTO `countries_es` (`id`, `country_code`, `country_name`) VALUES
(1, 'uk', 'Reino Unido'),
(2, 'de', 'Alemania'),
(3, 'fr', 'Francia'),
(4, 'es', 'España'),
(5, 'jp', 'Japón');

-- --------------------------------------------------------

--
-- Table structure for table `countries_fr`
--

CREATE TABLE `countries_fr` (
  `id` int(11) NOT NULL,
  `country_code` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries_fr`
--

INSERT INTO `countries_fr` (`id`, `country_code`, `country_name`) VALUES
(1, 'uk', 'Royaume-Uni'),
(2, 'de', 'Allemagne'),
(3, 'fr', 'France'),
(4, 'es', 'Espagne'),
(5, 'jp', 'Japon');

-- --------------------------------------------------------

--
-- Table structure for table `countries_jp`
--

CREATE TABLE `countries_jp` (
  `id` int(11) NOT NULL,
  `country_code` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries_jp`
--

INSERT INTO `countries_jp` (`id`, `country_code`, `country_name`) VALUES
(1, 'uk', 'イギリス'),
(2, 'de', 'ドイツ'),
(3, 'fr', 'フランス'),
(4, 'es', 'スペイン'),
(5, 'jp', '日本');

-- --------------------------------------------------------

--
-- Table structure for table `countries_uk`
--

CREATE TABLE `countries_uk` (
  `id` int(11) NOT NULL,
  `country_code` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries_uk`
--

INSERT INTO `countries_uk` (`id`, `country_code`, `country_name`) VALUES
(1, 'uk', 'United Kingdom'),
(2, 'de', 'Germany'),
(3, 'fr', 'France'),
(4, 'es', 'Spain'),
(5, 'jp', 'Japan');

-- --------------------------------------------------------

--
-- Table structure for table `general_translations`
--

CREATE TABLE `general_translations` (
  `id` int(11) NOT NULL,
  `lang_select_title` varchar(255) NOT NULL,
  `submit_button` varchar(255) NOT NULL,
  `right_answer` varchar(255) NOT NULL,
  `wrong_answer` varchar(255) NOT NULL,
  `try_again_text` varchar(255) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `next_question` varchar(255) NOT NULL,
  `finish` varchar(255) NOT NULL,
  `prompt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_translations`
--

INSERT INTO `general_translations` (`id`, `lang_select_title`, `submit_button`, `right_answer`, `wrong_answer`, `try_again_text`, `language_code`, `next_question`, `finish`, `prompt`) VALUES
(1, 'Please Select A Language', 'Submit', 'Well Done!', 'Sorry!', 'Please Try Again.', 'uk', 'Next Question', 'Finish', 'Please select an answer to continue'),
(2, ' Bitte wähle eine Sprache', 'Einreichen', 'Gut gemacht!', 'Es tut uns leid!', 'Bitte versuche es erneut.', 'de', 'Nächste Frage', 'Fertig', 'Bitte wählen Sie eine Antwort aus'),
(3, 'Sélectionnez une langue', 'Soumettre', 'Bien joué!', 'Pardon!', 'Veuillez réessayer.', 'fr', 'Question suivante', 'Terminer', 'Sélectionnez une réponse pour continuer'),
(4, 'Por favor, seleccione un idioma', 'Enviar', '¡Bien hecho!', '¡Lo siento!', 'Inténtalo de nuevo.', 'es', 'Próxima pregunta', 'Terminar', 'Por favor, seleccione una respuesta para continuar'),
(5, '言語を選択してください', '提出する', 'よくやった！', 'ごめんなさい！', 'もう一度お試しください。', 'jp', '次の問題', 'フィニッシュ', '続行するには回答を選択してください');

-- --------------------------------------------------------

--
-- Table structure for table `questions_de`
--

CREATE TABLE `questions_de` (
  `question_text` varchar(255) NOT NULL,
  `success_text` varchar(255) NOT NULL,
  `more_information_paragraph_one` varchar(255) NOT NULL,
  `more_information_paragraph_two` varchar(255) NOT NULL,
  `question_number` int(11) NOT NULL,
  `more_information_title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions_de`
--

INSERT INTO `questions_de` (`question_text`, `success_text`, `more_information_paragraph_one`, `more_information_paragraph_two`, `question_number`, `more_information_title`, `image`) VALUES
('Wo befindet sich der Hauptsitz von Gomo?', 'Gomos Hauptsitz ist in Brighton.', 'Brighton ist ein Badeort an der Südküste Englands. Es ist Teil der zeremoniellen Grafschaft von East Sussex, innerhalb der historischen Grafschaft von Sussex.', 'Der archäologische Nachweis der Siedlung in der Gegend geht zurück auf die Bronzezeit, römische und angelsächsische Perioden.', 1, 'Fakten über Brighton', 'map'),
('Was ist heute der größte Säugetier?', 'Der Blue Whale ist heute der größte lebende Säugetier', 'Der blaue Wal (Balaenoptera musculus) ist ein marine Säugetier der Baleenwale (Mysticeti).', 'Es ist das größte Tier, das jemals existiert hat.', 2, 'Blue Whale Fakten!', 'animals');

-- --------------------------------------------------------

--
-- Table structure for table `questions_es`
--

CREATE TABLE `questions_es` (
  `question_text` varchar(255) NOT NULL,
  `success_text` varchar(255) NOT NULL,
  `more_information_paragraph_one` varchar(255) NOT NULL,
  `more_information_paragraph_two` varchar(255) NOT NULL,
  `question_number` int(11) NOT NULL,
  `more_information_title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions_es`
--

INSERT INTO `questions_es` (`question_text`, `success_text`, `more_information_paragraph_one`, `more_information_paragraph_two`, `question_number`, `more_information_title`, `image`) VALUES
('¿Dónde se encuentra la sede de Gomo?', 'La oficina central de Gomo está en Brighton.', 'Brighton es un balneario en la costa sur de Inglaterra. Es parte del condado ceremonial de Sussex del este, dentro del condado histórico de Sussex.', 'La evidencia arqueológica del establecimiento en el área se remonta a los períodos de la edad de bronce, romano y anglosajón.', 1, 'Datos sobre Brighton', 'map'),
('¿Cuál es el mamífero más grande vivo hoy?', 'La ballena azul es el mamífero vivo más grande hoy', 'La ballena azul (Balaenoptera musculus) es un mamífero marino perteneciente a las ballenas ballenas (Mysticeti).', 'Es el animal más grande que se sabe que ha existido.', 2, '¡Hechos de la ballena azul!', 'animals');

-- --------------------------------------------------------

--
-- Table structure for table `questions_fr`
--

CREATE TABLE `questions_fr` (
  `question_text` varchar(255) NOT NULL,
  `success_text` varchar(255) NOT NULL,
  `more_information_paragraph_one` varchar(255) NOT NULL,
  `more_information_paragraph_two` varchar(255) NOT NULL,
  `question_number` int(11) NOT NULL,
  `more_information_title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions_fr`
--

INSERT INTO `questions_fr` (`question_text`, `success_text`, `more_information_paragraph_one`, `more_information_paragraph_two`, `question_number`, `more_information_title`, `image`) VALUES
('Où se trouve le siège social de Gomo?', 'Le siège social de Gomo se trouve à Brighton.', 'Brighton est une station balnéaire sur la côte sud de l\'Angleterre. Il fait partie du comté cérémonial de East Sussex, dans le comté historique de Sussex.', 'La preuve archéologique du règlement dans la région remonte à l\'époque du bronze, aux périodes romaine et anglo-saxonne.', 1, 'Faits sur Brighton', 'map'),
('Quel est le plus grand mammifère vivant aujourd\'hui?', 'La baleine bleue est le plus grand mammifère vivant aujourd\'hui', 'La baleine bleue (Balaenoptera musculus) est un mammifère marin appartenant aux baleines à baleines (Mysticeti).', 'C\'est le plus grand animal connu pour exister.', 2, 'Faits sur les baleines bleues!', 'animals');

-- --------------------------------------------------------

--
-- Table structure for table `questions_jp`
--

CREATE TABLE `questions_jp` (
  `question_text` varchar(255) NOT NULL,
  `success_text` varchar(255) NOT NULL,
  `more_information_paragraph_one` varchar(255) NOT NULL,
  `more_information_paragraph_two` varchar(255) NOT NULL,
  `question_number` int(11) NOT NULL,
  `more_information_title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions_jp`
--

INSERT INTO `questions_jp` (`question_text`, `success_text`, `more_information_paragraph_one`, `more_information_paragraph_two`, `question_number`, `more_information_title`, `image`) VALUES
('ゴモの本社はどこにありますか？', 'ゴモの本社はブライトンにあります。', 'ブライトンはイングランドの南海岸にある海辺のリゾートです。それはサセックスの歴史的な郡内のイースト・サセックスの儀式郡の一部です。', 'この地域における和解の考古学的証拠は、青銅器時代、ローマ時代およびアングロサクソン期にまでさかのぼります。', 1, 'ブライトンについて', 'map'),
('今日、最大の哺乳動物は何ですか？', '青いクジラは今日最大の生きた哺乳動物です', '青色のクジラ（Balaenoptera musculus）は、バリス・クジラ（Mysticeti）に属する海洋哺乳類です。', 'これまで存在していたことで知られている最大の動物です。', 2, '青いクジラの事実！', 'animals');

-- --------------------------------------------------------

--
-- Table structure for table `questions_uk`
--

CREATE TABLE `questions_uk` (
  `question_text` varchar(255) NOT NULL,
  `success_text` varchar(255) NOT NULL,
  `more_information_paragraph_one` varchar(255) NOT NULL,
  `more_information_paragraph_two` varchar(255) NOT NULL,
  `question_number` int(11) NOT NULL,
  `more_information_title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions_uk`
--

INSERT INTO `questions_uk` (`question_text`, `success_text`, `more_information_paragraph_one`, `more_information_paragraph_two`, `question_number`, `more_information_title`, `image`) VALUES
('Where is Gomo\'s head office located?', 'Gomos Head Office is in Brighton.', 'Brighton is a seaside resort on the south coast of England. It is part of the ceremonial county of East Sussex, within the historic county of Sussex.', 'Archaeological evidence of settlement in the area dates back to the Bronze Age, Roman and Anglo-Saxon periods.', 1, 'Facts About Brighton', 'map'),
('What is the largest Mammal alive today?', 'The Blue Whale is the largest living mammal today', 'The blue whale (Balaenoptera musculus) is a marine mammal belonging to the baleen whales (Mysticeti).', 'It is the largest animal known to have ever existed.', 2, 'Blue Whale Facts!', 'animals');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers_de`
--
ALTER TABLE `answers_de`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `answers_es`
--
ALTER TABLE `answers_es`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `answers_fr`
--
ALTER TABLE `answers_fr`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `answers_jp`
--
ALTER TABLE `answers_jp`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `answers_uk`
--
ALTER TABLE `answers_uk`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `countries_de`
--
ALTER TABLE `countries_de`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries_es`
--
ALTER TABLE `countries_es`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries_fr`
--
ALTER TABLE `countries_fr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries_jp`
--
ALTER TABLE `countries_jp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries_uk`
--
ALTER TABLE `countries_uk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_translations`
--
ALTER TABLE `general_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions_de`
--
ALTER TABLE `questions_de`
  ADD PRIMARY KEY (`question_number`);

--
-- Indexes for table `questions_es`
--
ALTER TABLE `questions_es`
  ADD PRIMARY KEY (`question_number`);

--
-- Indexes for table `questions_fr`
--
ALTER TABLE `questions_fr`
  ADD PRIMARY KEY (`question_number`);

--
-- Indexes for table `questions_jp`
--
ALTER TABLE `questions_jp`
  ADD PRIMARY KEY (`question_number`);

--
-- Indexes for table `questions_uk`
--
ALTER TABLE `questions_uk`
  ADD PRIMARY KEY (`question_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers_de`
--
ALTER TABLE `answers_de`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `answers_es`
--
ALTER TABLE `answers_es`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `answers_fr`
--
ALTER TABLE `answers_fr`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `answers_jp`
--
ALTER TABLE `answers_jp`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `answers_uk`
--
ALTER TABLE `answers_uk`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `countries_de`
--
ALTER TABLE `countries_de`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `countries_es`
--
ALTER TABLE `countries_es`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `countries_fr`
--
ALTER TABLE `countries_fr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `countries_jp`
--
ALTER TABLE `countries_jp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `countries_uk`
--
ALTER TABLE `countries_uk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `general_translations`
--
ALTER TABLE `general_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `questions_de`
--
ALTER TABLE `questions_de`
  MODIFY `question_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `questions_es`
--
ALTER TABLE `questions_es`
  MODIFY `question_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `questions_fr`
--
ALTER TABLE `questions_fr`
  MODIFY `question_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `questions_jp`
--
ALTER TABLE `questions_jp`
  MODIFY `question_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `questions_uk`
--
ALTER TABLE `questions_uk`
  MODIFY `question_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;