<?php 
    // Get POST data
    $countryCode = $_GET['code'];
    // Connect to DB - Please enter the connection details of your server database here
    $servername = "localhost";
    $username = "root";
    $password = "root";
    $dbname = "devserve_gomo";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
    //Perform SQL Queries
    $dropdownSql = "SELECT id, country_code, country_name FROM countries_".$countryCode;
    $dropdownResult = $conn->query($dropdownSql);

    $questionsSql = "SELECT questions_".$countryCode.".image, questions_".$countryCode.".question_text, questions_".$countryCode.".success_text, questions_".$countryCode.".more_information_title, questions_".$countryCode.".more_information_paragraph_one, questions_".$countryCode.".more_information_paragraph_two, questions_".$countryCode.".question_number, questions_".$countryCode.".question_number, answers_".$countryCode.".answer_id, answers_".$countryCode.".answer_text FROM answers_".$countryCode." INNER JOIN questions_".$countryCode." ON questions_".$countryCode.".question_number=answers_".$countryCode.".question_id;";
    $questionsResult = $conn->query($questionsSql);

    $answersSql = "SELECT * FROM answers_".$countryCode;
    $answersResult = $conn->query($answersSql);

    $translationsSql = 'SELECT * FROM general_translations WHERE language_code = "'.$countryCode.'"';
    $translationsResult = $conn->query($translationsSql);

    //Prepare results array and language selector dropdown html
    $translations = array();
    $dropdownHtml = "<select id = 'languages' onchange=$('#gomo-survey').load('translate.php?code='+$(this).val())>";

    //Create dropdown containing each language in the DB
    if ($dropdownResult->num_rows > 0) {
        while($row = $dropdownResult->fetch_assoc()) {

            $rowCode=$row["country_code"];
            $dropdownHtml.='<option value="'.$rowCode.'"';

            if($countryCode==$rowCode){
                $dropdownHtml.=' selected="selected"';
            }

            $dropdownHtml.='>'.$row["country_name"].'</option>'; 
        }
        $dropdownHtml.="</select>";
    } else {
        echo $dropdownResult;
    }
    
    //Populate the $questions array with an array for each question and it's translations.
    if ($questionsResult->num_rows > 0) {
        // output data of each row
        while($row = $questionsResult->fetch_assoc()) {
            $questions[$row["question_number"]-1][] = array("questionText" => $row["question_text"], "questionImage" => $row["image"],"successText" => $row["success_text"],"paragraphOne" => $row["more_information_paragraph_one"],"paragraphTwo" => $row["more_information_paragraph_two"],"infoTitle" => $row["more_information_title"],"questionNumber" => $row["question_number"],"answerText" => $row["answer_text"],"answerNumber" => $row["answer_id"]);
        }

    } else {
        echo "No Questions Found";
    }
    //Populate the $translations array with strings from the DB
    if ($translationsResult->num_rows > 0) {
        while($row = $translationsResult->fetch_assoc()) {
            $translations = array("code" => $row["language_code"],"langTitle" => $row["lang_select_title"],"submit" => $row["submit_button"],"rightAnswer" => $row["right_answer"],"wrongAnswer" => $row["wrong_answer"],"tryAgain" => $row["try_again_text"],"nextQuestion" => $row["next_question"],"finish" => $row["finish"],"prompt" => $row["prompt"]);
            }
    } else {
        echo "No Translations Found";
    }
    //Close the DB connection
    $conn->close();
?>
<!--Output the entire survey html, with translations -->
<div class="container main">
    <!-- Language dropdown -->
    <div id="language-select" class="main-content col-md-6 col-sm-12 center-block show">
        <h2 id="lang-title"><?php echo ($translations["langTitle"]); ?></h2>
        <div class="col-sm-12 center-block">
              <?php echo $dropdownHtml; ?>
        </div>
        <div class="col-sm-12 center-block">
          <button id="lang-submit" type="button" class="btn btn-success" onclick="startSurvey()" ><?php echo ($translations["submit"]); ?></button>
        </div>
    </div>
    <!-- Submit Button -->
    <div id="survey" class="main-content col-md-6 col-sm-12 center-block">

<?php
    //Loop through the questions, creating a unique form for each one
    $questionCount=count($questions);
    $last = 0;

    foreach($questions as $question){
        //If it is the last question, make a note
        $questionNumber = $question[0]["questionNumber"];
        if($questionNumber==$questionCount){
            $last = 1;
        }
?>
        <!-- Start Form -->
        <form id="form-<?php echo($questionNumber)?>" method="post" action="result.php">
            <fieldset>
                <legend id="question"><?php echo ($question[0]["questionText"]); ?></legend>
                <div class="row">
                    <div class="col-xs-6">
<?php
                        //Loop through each answer and create a radio button
                        foreach($question as $answer){
                            echo('<input type="radio" name="answer" value="'.$answer['answerNumber'].'" required> '.$answer['answerText'].'<br />');
                        }
?>                      <!-- Submit Button -->
                        <button class="btn btn-success" type="button" onclick="submitAnswer('form-<?php echo ($questionNumber); ?>', <?php echo ($question[0]["questionNumber"]); ?>, '<?php echo (mysql_real_escape_string($question[0]["successText"])); ?>')">
                            <?php echo ($translations["submit"]); ?>
                        </button>
                    </div>
                    <!-- Question image -->
                    <div class="col-xs-6">
                        <img class="map" src="img/<?php echo ($question[0]["questionImage"]); ?>.jpg"/>
                    </div>
                </div>
                
                <hr>
                <!-- More Info Container -->
                <div class="row more-information">
                    <h3>
                        <?php echo ($question[0]["infoTitle"]); ?>
                    </h3>
                    <div class="col-xs-6">
                        <p>
                            <?php echo ($question[0]["paragraphOne"]); ?>
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            <?php echo ($question[0]["paragraphTwo"]); ?>
                        </p>
                    </div>
                    <!-- Next Question Button -->
                    <div class="col-xs-12">
                        <button class="btn btn-success" type="button" onclick="nextQuestion(<?php echo ($questionNumber.','.$last); ?>)">   
<?php
                            //Display a next question button if there are more or a finish button if not
                            if($questionNumber!=$questionCount){
                                echo(mysql_real_escape_string($translations["nextQuestion"]));
                            }    
                            else{
                                echo(mysql_real_escape_string($translations["finish"]));
                            }
?>
                        </button>
                    </div>
                </div>
            </fieldset>
        </form>
<?php 
    }
?>
    </div>
</div>
<!-- Footer -->
<div class="container footer">
    <hr>
    <footer>
        <p>&copy; Philip Pemberton 2017</p>
    </footer>
</div>
<!-- Popup to display notifications -->
<div id="results">
    <div class="main-content col-md-6 col-sm-12 center-block">
        <button type="button" class="btn btn-success close" onclick="$('#results').removeClass('show');">X</button>
        <h2 id="result-title-success"><?php echo ($translations["rightAnswer"]); ?></h2>
        <h2 id="result-title-fail"><?php echo ($translations["wrongAnswer"]); ?></h2>
        <div class="col-sm-12 center-block">
            <p id="result-message-success"></p>
            <p id="result-message-fail"><?php echo ($translations["tryAgain"]); ?></p>
            <p id="result-message-prompt"><?php echo ($translations["prompt"]); ?></p>
        </div>
    </div>
</div>