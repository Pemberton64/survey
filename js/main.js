function startSurvey(){
        $('#language-select').removeClass('show');
        $('#survey').addClass('show');
        $('#form-1').addClass('show');
        $('.more-information').removeClass('show');
}

// Set up an event listener for the contact form.
    function submitAnswer(formId, questionNumber, successText) {
        // Serialize the form data.
        var formData = $("#"+formId).serialize();
        if(formData){
            var formData = formData+"&question="+questionNumber;
            // Submit the form using AJAX.
            $.ajax({
                type: 'POST',
                url: $("#"+formId).attr('action'),
                data: formData
            }).done(function(response) {
                if(response=="true"){
                    $('#result-message-success').text(successText);
                    $('#'+formId+' .more-information').addClass('show');
                    $('#result-title-success').addClass('show');
                    $('#result-message-success').addClass('show');
                    $('#result-title-fail').removeClass('show');
                    $('#result-message-fail').removeClass('show');
                    $('#result-message-prompt').removeClass('show');
                }
                else{
                    $('#'+formId+' .more-information').removeClass('show');
                    $('#result-title-fail').addClass('show');
                    $('#result-message-fail').addClass('show');
                    $('#result-title-success').removeClass('show');
                    $('#result-message-success').removeClass('show');
                    $('#result-message-prompt').removeClass('show');
                }

            }).fail(function(data) {
                $("#result-title").text("Sorry, there has been a problem!");
                // Set the message text.
                if (data.responseText !== '') {
                    $("#result-message").text(data.responseText);
                } else {
                    $("#result-message").text('Oops! An error occured and your answer could not be submitted.');
                }
            });
        }
        else{
            $('#result-message-success').text("Please select an answer to proceed");
            $('#result-message-prompt').addClass('show');
            $('#result-title-success').removeClass('show');
            $('#result-message-success').removeClass('show');
            $('#result-title-fail').removeClass('show');
            $('#result-message-fail').removeClass('show');
        }
        $('#results').addClass('show');
    }
    function nextQuestion(questionNumber, last) {

        $('#form-'+(questionNumber)).removeClass('show');
        var nextQuestion=(questionNumber+1);
        if(!last){
            $('#form-'+(nextQuestion)).addClass('show');
        }
        else{
            $('#language-select').addClass('show');
            $('#survey').removeClass('show');
        }
    }

$( document ).ready(function() {
    $( "#gomo-survey" ).load('translate.php?code=' + "uk");
});
