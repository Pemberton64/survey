<?php  
    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $_question = $_POST["question"];
        $_answer = $_POST["answer"];

        // Check that data was sent.
        if ( empty($_answer)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Oops! There was a problem with your submission. Please complete the form and try again.";
            exit;
        }

        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbname = "devserve_gomo";

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        
        $sqlResult = "SELECT * FROM answers_uk WHERE answer_id = ".$_answer." AND solution = 1 AND question_id = ".$_question.";";
        $solution = $conn->query($sqlResult);

        if ($solution->num_rows > 0) {
            http_response_code(200);
            echo("true");
        }
        else{
            http_response_code();
            echo("false");
        }
        $conn->close();
    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }

?>